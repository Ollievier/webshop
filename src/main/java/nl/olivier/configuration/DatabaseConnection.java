package nl.olivier.configuration;

import java.sql.*;

/**
 * Creates the initial database connection.
 */
public final class DatabaseConnection
{
	private static Connection connection;

	/**
	 * Default constructor.
	 */
	private DatabaseConnection ()
	{

	}

	/**
	 * Fetch the current {@link Connection}.
	 *
	 * @return The {@link Connection}.
	 */
	public static Connection fetchConnection ()
	{
		return connection != null ? connection : initializeConnection();
	}


	/**
	 * Closes the current active {@link Connection}.
	 */
	private static void closeConnection ()
	{
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException exception) {
				exception.printStackTrace();
			}
		}
	}


	/**
	 * Initializes a {@link Connection} with the database.
	 *
	 * @return the current {@link Connection}.
	 */
	private static Connection initializeConnection ()
	{
		Connection connection = null;
		try {
			String userName = "webshop";
			String password = "webshop";
			String url      = "jdbc:mysql://localhost:3306/winkel";
			connection = DriverManager.getConnection(url, userName, password);
		} catch (Exception exception) {
			System.err.println("Cannot connect to database server");
			exception.printStackTrace();
		}

		return connection;
	}

	/**
	 * Closes all Objects.
	 */
	public static void closeAll (ResultSet resultSet, Statement statement, PreparedStatement preparedStatement)
	{
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			if (statement != null) {
				statement.close();
			}
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			DatabaseConnection.closeConnection();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

}
