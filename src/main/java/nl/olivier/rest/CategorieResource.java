package nl.olivier.rest;


import nl.olivier.models.Categorie;
import nl.olivier.services.CategorieService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Resource for {@link Categorie}s.
 */
@Path("/Categorie")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategorieResource
{
	@EJB
	private CategorieService categorieService;

	@GET
	public List<Categorie> fetchCategories ()
	{
		return categorieService.fetchCategories();
	}

	@GET
	@Path("/{id}")
	public Response fetchCategorie (@PathParam("id") int categorieId)
	{
		Categorie categorie = categorieService.fetchCategorie(categorieId);
		if (categorie.getId() > 0) {
			return Response
					.status(Response.Status.OK)
					.type(MediaType.APPLICATION_JSON)
					.entity(categorie)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.build();
		}
	}

	@POST
	public Response createCategorie (Categorie categorie)
	{
		Categorie result = categorieService.createCategorie(categorie);
		if (result.getId() > 0) {
			return Response
					.status(Response.Status.CREATED)
					.type(MediaType.APPLICATION_JSON)
					.entity(result)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}

	@PUT
	@Path("/{id}")
	public Response updateCategorie (Categorie categorie, @PathParam("id") int categorieId)
	{
		Categorie result = categorieService.updateCategorie(categorie.setId(categorieId));
		if (categorieService.isEqual(categorie, result)) {
			return Response
					.status(Response.Status.OK)
					.type(MediaType.APPLICATION_JSON)
					.entity(result)
					.build();
		}
		else {
			return Response
					.status(Response.Status.NOT_MODIFIED)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}

	@DELETE
	@Path("/{id}")
	public Response deleteCategorie (@PathParam("id") int categorieId)
	{
		if (categorieService.deleteCategorie(categorieId)) {
			return Response
					.status(Response.Status.NO_CONTENT)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}
}
