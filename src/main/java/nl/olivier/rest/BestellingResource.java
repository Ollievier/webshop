package nl.olivier.rest;


import nl.olivier.models.Bestelling;
import nl.olivier.services.BestellingService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Resource for {@link Bestelling}en.
 */
@Path("/Bestelling")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BestellingResource
{
	@EJB
	private BestellingService bestellingService;

	@GET
	public List<Bestelling> fetchBestellingen ()
	{
		return bestellingService.fetchBestellingen();
	}

	@GET
	@Path("/{id}")
	public Response fetchBestelling (@PathParam("id") int bestellingId)
	{
		Bestelling bestelling = bestellingService.fetchBestelling(bestellingId);
		if (bestelling.getId() > 0) {
			return Response
					.status(Response.Status.OK)
					.type(MediaType.APPLICATION_JSON)
					.entity(bestelling)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.build();
		}
	}

	@POST
	public Response createBestelling (Bestelling bestelling)
	{
		Bestelling result = bestellingService.createBestelling(bestelling);
		if (result.getId() > 0) {
			return Response
					.status(Response.Status.CREATED)
					.type(MediaType.APPLICATION_JSON)
					.entity(result)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}

	@PUT
	@Path("/{id}")
	public Response updateBestelling (Bestelling bestelling, @PathParam("id") int bestellingId)
	{
		Bestelling result = bestellingService.updateBestelling(bestelling.setId(bestellingId));
		if (bestellingService.isEqual(bestelling, result)) {
			return Response
					.status(Response.Status.OK)
					.type(MediaType.APPLICATION_JSON)
					.entity(result)
					.build();
		}
		else {
			return Response
					.status(Response.Status.NOT_MODIFIED)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}

	@DELETE
	@Path("/{id}")
	public Response deleteBestelling (@PathParam("id") int bestellingId)
	{
		if (bestellingService.deleteBestelling(bestellingId)) {
			return Response
					.status(Response.Status.NO_CONTENT)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}
}
