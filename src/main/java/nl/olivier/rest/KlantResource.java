package nl.olivier.rest;


import nl.olivier.models.Klant;
import nl.olivier.services.KlantService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Resource for {@link Klant}en.
 */
@Path("/Klant")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class KlantResource
{
	@EJB
	private KlantService klantService;

	@GET
	public List<Klant> fetchKlants ()
	{
		return klantService.fetchKlanten();
	}

	@GET
	@Path("/{id}")
	public Response fetchKlant (@PathParam("id") int klantId)
	{
		Klant klant = klantService.fetchKlant(klantId);
		if (klant.getId() > 0) {
			return Response
					.status(Response.Status.OK)
					.type(MediaType.APPLICATION_JSON)
					.entity(klant)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.build();
		}
	}

	@POST
	public Response createKlant (Klant klant)
	{
		Klant result = klantService.createKlant(klant);
		if (result.getId() > 0) {
			return Response
					.status(Response.Status.CREATED)
					.type(MediaType.APPLICATION_JSON)
					.entity(result)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}

	@PUT
	@Path("/{id}")
	public Response updateKlant (Klant klant, @PathParam("id") int klantId)
	{
		Klant result = klantService.updateKlant(klant.setId(klantId));
		if (klantService.isEqual(klant, result)) {
			return Response
					.status(Response.Status.OK)
					.type(MediaType.APPLICATION_JSON)
					.entity(result)
					.build();
		}
		else {
			return Response
					.status(Response.Status.NOT_MODIFIED)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}

	@DELETE
	@Path("/{id}")
	public Response deleteKlant (@PathParam("id") int klantId)
	{
		if (klantService.deleteKlant(klantId)) {
			return Response
					.status(Response.Status.NO_CONTENT)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}
}
