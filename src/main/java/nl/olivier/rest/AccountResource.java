package nl.olivier.rest;


import nl.olivier.models.Account;
import nl.olivier.services.AccountService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Resource for {@link Account}s.
 */
@Path("/Account")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AccountResource
{
	@EJB
	private AccountService accountService;

	@GET
	public List<Account> fetchAccounts ()
	{
		return accountService.fetchAccounts();
	}

	@GET
	@Path("/{id}")
	public Response fetchAccount (@PathParam("id") int accountId)
	{
		Account account = accountService.fetchAccount(accountId);
		if (account.getId() > 0) {
			return Response
					.status(Response.Status.OK)
					.type(MediaType.APPLICATION_JSON)
					.entity(account)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.build();
		}
	}

	@POST
	public Response createAccount (Account account)
	{
		Account result = accountService.createAccount(account);
		if (result.getId() > 0) {
			return Response
					.status(Response.Status.CREATED)
					.type(MediaType.APPLICATION_JSON)
					.entity(result)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}

	@PUT
	@Path("/{id}")
	public Response updateAccount (Account account, @PathParam("id") int accountId)
	{
		Account result = accountService.updateAccount(account.setId(accountId));
		if (accountService.isEqual(account, result)) {
			return Response
					.status(Response.Status.OK)
					.type(MediaType.APPLICATION_JSON)
					.entity(result)
					.build();
		}
		else {
			return Response
					.status(Response.Status.NOT_MODIFIED)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}

	@DELETE
	@Path("/{id}")
	public Response deleteAccount (@PathParam("id") int accountId)
	{
		if (accountService.deleteAccount(accountId)) {
			return Response
					.status(Response.Status.NO_CONTENT)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}
}
