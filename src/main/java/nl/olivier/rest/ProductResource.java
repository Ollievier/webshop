package nl.olivier.rest;


import nl.olivier.models.Categorie;
import nl.olivier.models.Product;
import nl.olivier.services.ProductService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Resource for {@link Product}s.
 */
@Path("/Product")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProductResource
{
	@EJB
	private ProductService productService;

	@GET
	public List<Product> fetchProducts ()
	{
		return productService.fetchProducts();
	}

	@GET
	@Path("/{id}")
	public Response fetchProduct (@PathParam("id") int productId)
	{
		Product product = productService.fetchProduct(productId);
		if (product.getId() > 0) {
			return Response
					.status(Response.Status.OK)
					.type(MediaType.APPLICATION_JSON)
					.entity(product)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.build();
		}
	}

	@POST
	public Response createProduct (Product product)
	{
		Product result = productService.createProduct(product);
		if (result.getId() > 0) {
			return Response
					.status(Response.Status.CREATED)
					.type(MediaType.APPLICATION_JSON)
					.entity(result)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}

	@PUT
	@Path("/{id}")
	public Response updateProduct (Product product, @PathParam("id") int productId)
	{
		Product result = productService.updateProduct(product.setId(productId));
		if (productService.isEqual(product, result)) {
			return Response
					.status(Response.Status.OK)
					.type(MediaType.APPLICATION_JSON)
					.entity(result)
					.build();
		}
		else {
			return Response
					.status(Response.Status.NOT_MODIFIED)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}


	@DELETE
	@Path("/{id}")
	public Response deleteProduct (@PathParam("id") int productId)
	{
		if (productService.deleteProduct(productId)) {
			return Response
					.status(Response.Status.NO_CONTENT)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
		else {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}

	@POST
	@Path("/{id}/Categorie")
	public Response addCategorie (Categorie categorie, @PathParam("id") int productId)
	{
		Product result = productService.addCategorieToProduct(categorie, productId);
		if (result != null && result.getCategorieList().stream().anyMatch(x -> x.getId() == categorie.getId())) {
			return Response
					.status(Response.Status.CREATED)
					.type(MediaType.APPLICATION_JSON)
					.entity(result)
					.build();
		}
		else {
			return Response
					.status(Response.Status.NOT_MODIFIED)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}

	@DELETE
	@Path("/{id}/Categorie")
	public Response deleteCategorie (Categorie categorie, @PathParam("id") int productId)
	{
		Product result = productService.deleteCategorieFromProduct(categorie, productId);
		if (result.getCategorieList().stream().noneMatch(x -> x.getId() == categorie.getId())) {
			return Response
					.status(Response.Status.NOT_FOUND)
					.type(MediaType.APPLICATION_JSON)
					.entity(result)
					.build();
		}
		else {
			return Response
					.status(Response.Status.NOT_MODIFIED)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
	}
}
