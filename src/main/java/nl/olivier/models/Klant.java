package nl.olivier.models;

/**
 * Model for Klant
 */
public class Klant
{
	private int    id;
	private int    woonAdresId;
	private String naam;
	private byte[] afbeelding;

	public Klant ()
	{

	}

	public int getId ()
	{
		return id;
	}

	public Klant setId (int id)
	{
		this.id = id;
		return this;
	}

	public String getNaam ()
	{
		return naam;
	}

	public Klant setNaam (String naam)
	{
		this.naam = naam;
		return this;
	}

	public byte[] getAfbeelding ()
	{
		return afbeelding;
	}

	public Klant setAfbeelding (byte[] afbeelding)
	{
		this.afbeelding = afbeelding;
		return this;
	}

	public int getWoonAdresId ()
	{
		return woonAdresId;
	}

	public Klant setWoonAdresId (int woonAdresId)
	{
		this.woonAdresId = woonAdresId;
		return this;
	}
}
