package nl.olivier.models;

import java.sql.Date;

public class Account
{
	private int     id;
	private int     klantId;
	private int     factuurAdresId;
	private Date    openDatum;
	private boolean isActief;

	public Account ()
	{

	}

	public int getId ()
	{
		return id;
	}

	public Account setId (int id)
	{
		this.id = id;
		return this;
	}

	public int getKlantId ()
	{
		return klantId;
	}

	public Account setKlantId (int klantId)
	{
		this.klantId = klantId;
		return this;
	}

	public int getFactuurAdresId ()
	{
		return factuurAdresId;
	}

	public Account setFactuurAdresId (int factuurAdresId)
	{
		this.factuurAdresId = factuurAdresId;
		return this;
	}

	public Date getOpenDatum ()
	{
		return openDatum;
	}

	public Account setOpenDatum (Date openDatum)
	{
		this.openDatum = openDatum;
		return this;
	}

	public boolean isActief ()
	{
		return isActief;
	}

	public Account setActief (boolean actief)
	{
		isActief = actief;
		return this;
	}
}
