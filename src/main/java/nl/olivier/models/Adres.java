package nl.olivier.models;

/**
 * Model for a Address.
 */
public class Adres
{
	private int    id;
	private int    klantId;
	private String straat;
	private String huisnummer;

	public Adres ()
	{

	}

	public int getId ()
	{
		return id;
	}

	public Adres setId (int id)
	{
		this.id = id;
		return this;
	}

	public int getKlantId ()
	{
		return klantId;
	}

	public Adres setKlantId (int klantId)
	{
		this.klantId = klantId;
		return this;
	}

	public String getStraat ()
	{
		return straat;
	}

	public Adres setStraat (String straat)
	{
		this.straat = straat;
		return this;
	}

	public String getHuisnummer ()
	{
		return huisnummer;
	}

	public Adres setHuisnummer (String huisnummer)
	{
		this.huisnummer = huisnummer;
		return this;
	}
}
