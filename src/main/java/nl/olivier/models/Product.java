package nl.olivier.models;

import java.util.List;

/**
 * Class for Product-objects.
 */
public class Product
{
	private int             id;
	private String          naam;
	private double          prijs;
	private String          omschrijving;
	private byte[]          plaatje;
	private List<Categorie> categorieList;

	/**
	 * Default constructor.
	 */
	public Product ()
	{

	}

	public int getId ()
	{
		return id;
	}

	public Product setId (int id)
	{
		this.id = id;
		return this;
	}

	public String getNaam ()
	{
		return naam;
	}

	public Product setNaam (String naam)
	{
		this.naam = naam;
		return this;
	}

	public double getPrijs ()
	{
		return prijs;
	}

	public Product setPrijs (double prijs)
	{
		this.prijs = prijs;
		return this;
	}

	public String getOmschrijving ()
	{
		return omschrijving;
	}

	public Product setOmschrijving (String omschrijving)
	{
		this.omschrijving = omschrijving;
		return this;
	}

	public byte[] getPlaatje ()
	{
		return plaatje;
	}

	public Product setPlaatje (byte[] plaatje)
	{
		this.plaatje = plaatje;
		return this;
	}

	public List<Categorie> getCategorieList ()
	{
		return categorieList;
	}

	public Product setCategorieList (List<Categorie> categorieList)
	{
		this.categorieList = categorieList;
		return this;
	}
}
