package nl.olivier.models;

/**
 * A category for {@link Product}s.
 */
public class Categorie
{
	private int    id;
	private String naam;
	private String omschrijving;
	private byte[] plaatje;

	public Categorie ()
	{

	}

	public int getId ()
	{
		return id;
	}

	public Categorie setId (int id)
	{
		this.id = id;
		return this;
	}

	public String getNaam ()
	{
		return naam;
	}

	public Categorie setNaam (String naam)
	{
		this.naam = naam;
		return this;
	}

	public String getOmschrijving ()
	{
		return omschrijving;
	}

	public Categorie setOmschrijving (String omschrijving)
	{
		this.omschrijving = omschrijving;
		return this;
	}

	public byte[] getPlaatje ()
	{
		return plaatje;
	}

	public Categorie setPlaatje (byte[] plaatje)
	{
		this.plaatje = plaatje;
		return this;
	}
}
