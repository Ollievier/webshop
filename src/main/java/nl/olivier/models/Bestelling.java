package nl.olivier.models;

import java.sql.Date;

/**
 * Model for Bestelling.
 */
public class Bestelling
{
	private int     id;
	private int     afleverAdres;
	private int     accountId;
	private Date    bestelDatum;
	private boolean isBesteld;
	private double  totaalPrijs;

	public Bestelling ()
	{

	}

	public int getId ()
	{
		return id;
	}

	public Bestelling setId (int id)
	{
		this.id = id;
		return this;
	}

	public int getAfleverAdres ()
	{
		return afleverAdres;
	}

	public Bestelling setAfleverAdres (int afleverAdres)
	{
		this.afleverAdres = afleverAdres;
		return this;
	}

	public int getAccountId ()
	{
		return accountId;
	}

	public Bestelling setAccountId (int accountId)
	{
		this.accountId = accountId;
		return this;
	}

	public Date getBestelDatum ()
	{
		return bestelDatum;
	}

	public Bestelling setBestelDatum (Date bestelDatum)
	{
		this.bestelDatum = bestelDatum;
		return this;
	}

	public boolean isBesteld ()
	{
		return isBesteld;
	}

	public Bestelling setIsBesteld (boolean besteld)
	{
		isBesteld = besteld;
		return this;
	}

	public double getTotaalPrijs ()
	{
		return totaalPrijs;
	}

	public Bestelling setTotaalPrijs (double totaalPrijs)
	{
		this.totaalPrijs = totaalPrijs;
		return this;
	}
}
