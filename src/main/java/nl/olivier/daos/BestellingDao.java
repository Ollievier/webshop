package nl.olivier.daos;

import nl.olivier.configuration.DatabaseConnection;
import nl.olivier.models.Bestelling;

import javax.ejb.Stateless;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Dao dedicated to {@link Bestelling}en.
 */
@Stateless
public class BestellingDao
{
	private Statement         statement         = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet         resultSet         = null;

	/**
	 * Fetch all {@link Bestelling}en.
	 *
	 * @return A {@link List}<{@link Bestelling}>.
	 */
	public List<Bestelling> fetchBestellingen ()
	{
		List<Bestelling> bestellingList = new ArrayList<Bestelling>();
		try {
			statement = DatabaseConnection.fetchConnection().createStatement();
			resultSet = statement.executeQuery("SELECT * FROM bestelling");

			while (resultSet.next()) {
				bestellingList.add(extractBestelling(resultSet));
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}

		return bestellingList;
	}

	/**
	 * Fetch a {@link Bestelling} by its bestellingId.
	 *
	 * @param bestellingId The bestellingId.
	 * @return A {@link Bestelling}.
	 */
	public Bestelling fetchBestelling (int bestellingId)
	{
		Bestelling bestelling = new Bestelling();

		try {
			statement = DatabaseConnection.fetchConnection().createStatement();
			resultSet = statement.executeQuery("SELECT * FROM bestelling WHERE id =" + bestellingId);

			while (resultSet.next()) {
				bestelling = extractBestelling(resultSet);
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return bestelling;
	}

	/**
	 * Create a new {@link Bestelling}.
	 *
	 * @param newBestelling The {@link Bestelling}.
	 * @return The resulting {@link Bestelling}.
	 */
	public Bestelling createBestelling (Bestelling newBestelling)
	{
		Bestelling createdBestelling = new Bestelling();
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					"INSERT INTO bestelling (accountId, afleverAdresId, bestelDatum, isBesteld, totaalPrijs) VALUES (?, ?, ?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS
			);
			preparedStatement.setInt(1, newBestelling.getAccountId());
			preparedStatement.setInt(2, newBestelling.getAfleverAdres());
			preparedStatement.setDate(3, newBestelling.getBestelDatum());
			preparedStatement.setBoolean(4, newBestelling.isBesteld());
			preparedStatement.setDouble(5, newBestelling.getTotaalPrijs());

			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet != null && resultSet.next()) {
				createdBestelling = fetchBestelling(resultSet.getInt(1));
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return createdBestelling;
	}

	/**
	 * Update a {@link Bestelling}.
	 *
	 * @param updatedBestelling The {@link Bestelling}.
	 * @return The resulting {@link Bestelling}.
	 */
	public Bestelling updateBestelling (Bestelling updatedBestelling)
	{
		Bestelling result = new Bestelling();
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					"UPDATE bestelling SET bestelDatum = ?, isBesteld = ?, totaalPrijs = ? WHERE id = ?"
			);
			preparedStatement.setDate(1, updatedBestelling.getBestelDatum());
			preparedStatement.setBoolean(2, updatedBestelling.isBesteld());
			preparedStatement.setDouble(3, updatedBestelling.getTotaalPrijs());

			preparedStatement.executeUpdate();
			result = fetchBestelling(updatedBestelling.getId());

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	/**
	 * Delete a {@link Bestelling} by its ID.
	 *
	 * @param bestellingId The ID.
	 */
	public boolean deleteBestelling (int bestellingId)
	{
		boolean isDeleted = false;
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement("DELETE FROM bestelling WHERE id =?", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, bestellingId);
			isDeleted = preparedStatement.executeUpdate() == 1;

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return isDeleted;
	}

	/**
	 * Extracts a {@link Bestelling} from the {@link ResultSet}.
	 *
	 * @param resultSet The {@link ResultSet}.
	 * @return The extracted {@link Bestelling}.
	 * @throws SQLException Throws {@link SQLException} incase the {@link ResultSet} contains no entries.
	 */
	private Bestelling extractBestelling (ResultSet resultSet) throws SQLException
	{
		return new Bestelling()
				.setId(resultSet.getInt("id"))
				.setAfleverAdres(resultSet.getInt("afleverAdresId"))
				.setAccountId(resultSet.getInt("accountId"))
				.setIsBesteld(resultSet.getBoolean("isBesteld"))
				.setBestelDatum(resultSet.getDate("bestelDatum"))
				.setTotaalPrijs(resultSet.getDouble("totaalPrijs"));
	}

	/**
	 * Closes all the {@link ResultSet}, {@link Connection} and {@link Statement}.
	 */
	private void close ()
	{
		DatabaseConnection.closeAll(resultSet, statement, preparedStatement);
	}
}
