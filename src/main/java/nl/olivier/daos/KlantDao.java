package nl.olivier.daos;

import nl.olivier.configuration.DatabaseConnection;
import nl.olivier.models.Klant;

import javax.ejb.Stateless;
import javax.sql.rowset.serial.SerialBlob;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Dao dedicated to {@link Klant}en.
 */
@Stateless
public class KlantDao
{
	private static final String            QUERY_FULL_KLANT_PROFILE  = "INSERT INTO klant (naam, afbeelding, woonAdresId) VALUES (?, ?, ?)";
	private static final String            QUERY_BASIC_KLANT_PROFILE = "INSERT INTO klant (naam, afbeelding) VALUES (?, ?)";
	private              Statement         statement                 = null;
	private              PreparedStatement preparedStatement         = null;
	private              ResultSet         resultSet                 = null;

	/**
	 * Fetch all {@link Klant}en.
	 *
	 * @return A {@link List}<{@link Klant}>.
	 */
	public List<Klant> fetchKlanten ()
	{
		List<Klant> klantList = new ArrayList<Klant>();
		try {
			statement = DatabaseConnection.fetchConnection().createStatement();
			resultSet = statement.executeQuery("SELECT * FROM klant");

			while (resultSet.next()) {
				klantList.add(extractKlant(resultSet));
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}

		return klantList;
	}

	/**
	 * Fetch a {@link Klant} by its klantId.
	 *
	 * @param klantId The klantId.
	 * @return A {@link Klant}.
	 */
	public Klant fetchKlant (int klantId)
	{
		Klant klant = new Klant();

		try {
			statement = DatabaseConnection.fetchConnection().createStatement();
			resultSet = statement.executeQuery("SELECT * FROM klant WHERE id =" + klantId);

			while (resultSet.next()) {
				klant = extractKlant(resultSet);
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return klant;
	}

	/**
	 * Create a new {@link Klant}.
	 *
	 * @param newKlant The {@link Klant}.
	 * @return The resulting {@link Klant}.
	 */
	public Klant createKlant (Klant newKlant)
	{
		Klant createdKlant = new Klant();
		try {
			String query = newKlant.getWoonAdresId() > 0 ? QUERY_FULL_KLANT_PROFILE : QUERY_BASIC_KLANT_PROFILE;
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					query,
					Statement.RETURN_GENERATED_KEYS
			);
			preparedStatement.setString(1, newKlant.getNaam());
			preparedStatement.setBlob(2, newKlant.getAfbeelding() != null ? new SerialBlob(newKlant.getAfbeelding()) : null);
			if (newKlant.getWoonAdresId() > 0) {
				preparedStatement.setInt(3, newKlant.getWoonAdresId());
			}

			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet != null && resultSet.next()) {
				createdKlant = fetchKlant(resultSet.getInt(1));
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return createdKlant;
	}

	/**
	 * Update a {@link Klant}.
	 *
	 * @param updatedKlant The {@link Klant}.
	 * @return The resulting {@link Klant}.
	 */
	public Klant updateKlant (Klant updatedKlant)
	{
		Klant result = new Klant();
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					"UPDATE klant SET naam = ?, woonAdresId = ?, afbeelding = ? WHERE id = ?"
			);
			preparedStatement.setString(1, updatedKlant.getNaam());
			preparedStatement.setBlob(2, updatedKlant.getAfbeelding() != null ? new SerialBlob(updatedKlant.getAfbeelding()) : null);
			preparedStatement.setInt(3, updatedKlant.getWoonAdresId());
			preparedStatement.setInt(4, updatedKlant.getId());

			preparedStatement.executeUpdate();
			result = fetchKlant(updatedKlant.getId());

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	/**
	 * Delete a {@link Klant} by its ID.
	 *
	 * @param klantId The ID.
	 */
	public boolean deleteKlant (int klantId)
	{
		boolean isDeleted = false;
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement("DELETE FROM klant WHERE id =?", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, klantId);
			isDeleted = preparedStatement.executeUpdate() == 1;

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return isDeleted;
	}

	/**
	 * Extracts a {@link Klant} from the {@link ResultSet}.
	 *
	 * @param resultSet The {@link ResultSet}.
	 * @return The extracted {@link Klant}.
	 * @throws SQLException Throws {@link SQLException} incase the {@link ResultSet} contains no entries.
	 */
	private Klant extractKlant (ResultSet resultSet) throws SQLException
	{
		Blob blob = resultSet.getBlob("afbeelding");
		return new Klant()
				.setId(resultSet.getInt("id"))
				.setWoonAdresId(resultSet.getInt("woonAdresId"))
				.setNaam(resultSet.getString("naam"))
				.setAfbeelding(blob != null ? blob.getBytes(1, (int) blob.length()) : null);
	}

	/**
	 * Closes all the {@link ResultSet}, {@link Connection} and {@link Statement}.
	 */
	private void close ()
	{
		DatabaseConnection.closeAll(resultSet, statement, preparedStatement);
	}
}
