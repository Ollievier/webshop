package nl.olivier.daos;

import nl.olivier.configuration.DatabaseConnection;
import nl.olivier.models.Categorie;
import nl.olivier.models.Product;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.sql.rowset.serial.SerialBlob;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Dao dedicated to {@link Product}s.
 */
@Stateless
public class ProductDao
{
	@EJB
	private CategorieDao categorieDao;

	private Statement         statement         = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet         resultSet         = null;

	/**
	 * Fetch all {@link Product}'s.
	 *
	 * @return A {@link List}<{@link Product}>.
	 */
	public List<Product> fetchProducts ()
	{
		List<Product> productList = new ArrayList<Product>();
		try {
			statement = DatabaseConnection.fetchConnection().createStatement();
			resultSet = statement.executeQuery("SELECT * FROM product");

			while (resultSet.next()) {
				productList.add(extractProduct(resultSet));
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}

		return productList;
	}

	/**
	 * Fetch a {@link Product} by it ProductId.
	 *
	 * @param productId The ProductId.
	 * @return A {@link Product}.
	 */
	public Product fetchProduct (int productId)
	{
		Product product = new Product();

		try {
			statement = DatabaseConnection.fetchConnection().createStatement();
			resultSet = statement.executeQuery("SELECT * FROM product WHERE id =" + productId);

			while (resultSet.next()) {
				product = extractProduct(resultSet);
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return product;
	}

	/**
	 * Create a new {@link Product}.
	 *
	 * @param newProduct The {@link Product}.
	 * @return The resulting {@link Product}.
	 */
	public Product createProduct (Product newProduct)
	{
		Product createdProduct = new Product();
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					"INSERT INTO product (naam, prijs, omschrijving) VALUES (?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS
			);
			preparedStatement.setString(1, newProduct.getNaam());
			preparedStatement.setDouble(2, newProduct.getPrijs());
			preparedStatement.setString(3, newProduct.getOmschrijving());


			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet != null && resultSet.next()) {
				createdProduct = fetchProduct(resultSet.getInt(1));
			}

			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					"INSERT INTO productcategories (productId, categorieId) VALUES (?, ?)"
			);
			preparedStatement.setInt(1, createdProduct.getId());
			preparedStatement.setInt(2, 6);
			preparedStatement.execute();

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return createdProduct;
	}

	/**
	 * Update a {@link Product}.
	 *
	 * @param updatedProduct The {@link Product}.
	 * @return The resulting {@link Product}.
	 */
	public Product updateProduct (Product updatedProduct)
	{
		Product result = new Product();
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					"UPDATE product SET naam = ?, prijs = ?, omschrijving = ?, plaatje = ? WHERE id = ?"
			);
			preparedStatement.setString(1, updatedProduct.getNaam());
			preparedStatement.setDouble(2, updatedProduct.getPrijs());
			preparedStatement.setString(3, updatedProduct.getOmschrijving());
			preparedStatement.setBlob(4, updatedProduct.getPlaatje() != null ? new SerialBlob(updatedProduct.getPlaatje()) : null);
			preparedStatement.setInt(5, updatedProduct.getId());

			preparedStatement.executeUpdate();

			result = fetchProduct(updatedProduct.getId());


		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	/**
	 * Delete a {@link Product} by its ID.
	 *
	 * @param productId The ID.
	 */
	public boolean deleteProduct (int productId)
	{
		boolean isDeleted = false;
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement("DELETE FROM product WHERE id =?", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, productId);
			isDeleted = preparedStatement.executeUpdate() == 1;

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return isDeleted;
	}

	public Product addCategorieToProduct (int categorieId, int productId)
	{
		Product updatedProduct = new Product();
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					"INSERT INTO productcategories (productId, categorieId) VALUES (?, ?)",
					Statement.RETURN_GENERATED_KEYS
			);
			preparedStatement.setInt(1, productId);
			preparedStatement.setInt(2, categorieId);
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet != null && resultSet.next()) {
				updatedProduct = fetchProduct(productId);
			}


		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updatedProduct;
	}

	/**
	 * Deletes a categorie relaties between a {@link Product} and a {@link Categorie}.
	 */
	public Product deleteCategorieFromProduct (int categorieId, int productId)
	{
		boolean isDeleted = false;
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					"DELETE FROM productcategories WHERE productId = ? AND categorieId = ?",
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, productId);
			preparedStatement.setInt(2, categorieId);
			isDeleted = preparedStatement.executeUpdate() == 1;

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return fetchProduct(productId);
	}

	/**
	 * Extracts a {@link Product} from the {@link ResultSet}.
	 *
	 * @param resultSet The {@link ResultSet}.
	 * @return The extracted {@link Product}.
	 * @throws SQLException Throws {@link SQLException} incase the {@link ResultSet} contains no entries.
	 */
	private Product extractProduct (ResultSet resultSet) throws SQLException
	{
		Blob blob = resultSet.getBlob("plaatje");
		return new Product()
				.setId(resultSet.getInt("id"))
				.setNaam(resultSet.getString("naam"))
				.setOmschrijving(resultSet.getString("omschrijving"))
				.setPrijs(resultSet.getDouble("prijs"))
				.setPlaatje(blob != null ? blob.getBytes(1, (int) blob.length()) : null)
				.setCategorieList(categorieDao.fetchCategorieForProduct(resultSet.getInt("id")));
	}

	/**
	 * Closes all the {@link ResultSet}, {@link Connection} and {@link Statement}.
	 */
	private void close ()
	{
		DatabaseConnection.closeAll(resultSet, statement, preparedStatement);
	}
}
