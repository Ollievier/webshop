package nl.olivier.daos;

import nl.olivier.configuration.DatabaseConnection;
import nl.olivier.models.Account;

import javax.ejb.Stateless;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Dao dedicated to {@link Account}en.
 */
@Stateless
public class AccountDao
{
	private Statement         statement         = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet         resultSet         = null;

	/**
	 * Fetch all {@link Account}en.
	 *
	 * @return A {@link List}<{@link Account}>.
	 */
	public List<Account> fetchAccounts ()
	{
		List<Account> accountList = new ArrayList<Account>();
		try {
			statement = DatabaseConnection.fetchConnection().createStatement();
			resultSet = statement.executeQuery("SELECT * FROM account");

			while (resultSet.next()) {
				accountList.add(extractAccount(resultSet));
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}

		return accountList;
	}

	/**
	 * Fetch a {@link Account} by its accountId.
	 *
	 * @param accountId The accountId.
	 * @return A {@link Account}.
	 */
	public Account fetchAccount (int accountId)
	{
		Account account = new Account();

		try {
			statement = DatabaseConnection.fetchConnection().createStatement();
			resultSet = statement.executeQuery("SELECT * FROM account WHERE id =" + accountId);

			while (resultSet.next()) {
				account = extractAccount(resultSet);
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return account;
	}

	/**
	 * Create a new {@link Account}.
	 *
	 * @param newAccount The {@link Account}.
	 * @return The resulting {@link Account}.
	 */
	public Account createAccount (Account newAccount)
	{
		Account createdAccount = new Account();
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					"INSERT INTO account (klantId, factuurAdresId, openDatum, isActief) VALUES (?, ?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS
			);
			preparedStatement.setInt(1, newAccount.getId());
			preparedStatement.setInt(2, newAccount.getFactuurAdresId());
			preparedStatement.setDate(3, newAccount.getOpenDatum());
			preparedStatement.setInt(1, newAccount.getId());

			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet != null && resultSet.next()) {
				createdAccount = fetchAccount(resultSet.getInt(1));
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return createdAccount;
	}

	/**
	 * Update a {@link Account}.
	 *
	 * @param updatedAccount The {@link Account}.
	 * @return The resulting {@link Account}.
	 */
	public Account updateAccount (Account updatedAccount)
	{
		Account result = new Account();
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					"UPDATE account SET isActief = ? WHERE id = ?"
			);
			preparedStatement.setBoolean(1, updatedAccount.isActief());
			preparedStatement.setInt(2, updatedAccount.getId());

			preparedStatement.executeUpdate();
			result = fetchAccount(updatedAccount.getId());

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	/**
	 * Delete a {@link Account} by its ID.
	 *
	 * @param accountId The ID.
	 */
	public boolean deleteAccount (int accountId)
	{
		boolean isDeleted = false;
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement("DELETE FROM account WHERE id =?", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, accountId);
			isDeleted = preparedStatement.executeUpdate() == 1;

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return isDeleted;
	}

	/**
	 * Extracts a {@link Account} from the {@link ResultSet}.
	 *
	 * @param resultSet The {@link ResultSet}.
	 * @return The extracted {@link Account}.
	 * @throws SQLException Throws {@link SQLException} incase the {@link ResultSet} contains no entries.
	 */
	private Account extractAccount (ResultSet resultSet) throws SQLException
	{
		return new Account()
				.setId(resultSet.getInt("id"))
				.setKlantId(resultSet.getInt("klantId"))
				.setFactuurAdresId(resultSet.getInt("factuurAdresId"))
				.setOpenDatum(resultSet.getDate("openDatum"))
				.setActief(resultSet.getBoolean("isActief"));
	}

	/**
	 * Closes all the {@link ResultSet}, {@link Connection} and {@link Statement}.
	 */
	private void close ()
	{
		DatabaseConnection.closeAll(resultSet, statement, preparedStatement);
	}
}
