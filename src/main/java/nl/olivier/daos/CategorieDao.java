package nl.olivier.daos;

import nl.olivier.configuration.DatabaseConnection;
import nl.olivier.models.Categorie;
import nl.olivier.models.Product;

import javax.ejb.Stateless;
import javax.sql.rowset.serial.SerialBlob;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Dao dedicated to {@link Categorie}s.
 */
@Stateless
public class CategorieDao
{
	private Statement         statement         = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet         resultSet         = null;

	/**
	 * Fetch all {@link Categorie}'s.
	 *
	 * @return A {@link List}<{@link Categorie}>.
	 */
	public List<Categorie> fetchCategories ()
	{
		List<Categorie> categorieList = new ArrayList<Categorie>();
		try {
			statement = DatabaseConnection.fetchConnection().createStatement();
			resultSet = statement.executeQuery("SELECT * FROM Categorie");

			while (resultSet.next()) {
				categorieList.add(extractCategorie(resultSet));
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}

		return categorieList;
	}

	/**
	 * Fetch a {@link Categorie} by it CategorieId.
	 *
	 * @param categorieId The CategorieId.
	 * @return A {@link Categorie}.
	 */
	public Categorie fetchCategorie (int categorieId)
	{
		Categorie categorie = new Categorie();

		try {
			statement = DatabaseConnection.fetchConnection().createStatement();
			resultSet = statement.executeQuery("SELECT * FROM Categorie WHERE id =" + categorieId);

			while (resultSet.next()) {
				categorie = extractCategorie(resultSet);
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return categorie;
	}

	/**
	 * Fetch a {@link Categorie} List by its {@link Product}ID.
	 *
	 * @param productId The {@link Product} ID.
	 * @return A {@link List}<{@link Categorie}>.
	 */
	public List<Categorie> fetchCategorieForProduct (int productId)
	{
		List<Categorie> categorieList = new ArrayList<>();
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					"SELECT * FROM categorie cat INNER JOIN productcategories pc ON cat.id = pc.categorieId AND pc.productId = ?"
			);
			preparedStatement.setInt(1, productId);

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				categorieList.add(extractCategorie(resultSet));
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return categorieList;
	}


	/**
	 * Create a new {@link Categorie}.
	 *
	 * @param newCategorie The {@link Categorie}.
	 * @return The resulting {@link Categorie}.
	 */
	public Categorie createCategorie (Categorie newCategorie)
	{
		Categorie createdCategorie = new Categorie();
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					"INSERT INTO Categorie (naam, omschrijving, plaatje) VALUES (?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS
			);
			preparedStatement.setString(1, newCategorie.getNaam());
			preparedStatement.setString(2, newCategorie.getOmschrijving());
			preparedStatement.setBlob(3, newCategorie.getPlaatje() != null ? new SerialBlob(newCategorie.getPlaatje()) : null);


			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet != null && resultSet.next()) {
				createdCategorie = fetchCategorie(resultSet.getInt(1));
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return createdCategorie;
	}

	/**
	 * Update a {@link Categorie}.
	 *
	 * @param updatedCategorie The {@link Categorie}.
	 * @return The resulting {@link Categorie}.
	 */
	public Categorie updateCategorie (Categorie updatedCategorie)
	{
		Categorie result = new Categorie();
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement(
					"UPDATE categorie SET naam = ?, omschrijving = ?, plaatje = ? WHERE id = ?"
			);
			preparedStatement.setString(1, updatedCategorie.getNaam());
			preparedStatement.setString(2, updatedCategorie.getOmschrijving());
			preparedStatement.setBlob(3, updatedCategorie.getPlaatje() != null ? new SerialBlob(updatedCategorie.getPlaatje()) : null);
			preparedStatement.setInt(4, updatedCategorie.getId());

			preparedStatement.executeUpdate();
			result = fetchCategorie(updatedCategorie.getId());

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	/**
	 * Delete a {@link Categorie} by its ID.
	 *
	 * @param categorieId The ID.
	 */
	public boolean deleteCategorie (int categorieId)
	{
		boolean isDeleted = false;
		try {
			preparedStatement = DatabaseConnection.fetchConnection().prepareStatement("DELETE FROM Categorie WHERE id =?", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, categorieId);
			isDeleted = preparedStatement.executeUpdate() == 1;

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			close();
		}
		return isDeleted;
	}

	/**
	 * Extracts a {@link Categorie} from the {@link ResultSet}.
	 *
	 * @param resultSet The {@link ResultSet}.
	 * @return The extracted {@link Categorie}.
	 * @throws SQLException Throws {@link SQLException} incase the {@link ResultSet} contains no entries.
	 */
	private Categorie extractCategorie (ResultSet resultSet) throws SQLException
	{
		Blob blob = resultSet.getBlob("plaatje");
		return new Categorie()
				.setId(resultSet.getInt("id"))
				.setNaam(resultSet.getString("naam"))
				.setOmschrijving(resultSet.getString("omschrijving"))
				.setPlaatje(blob != null ? blob.getBytes(1, (int) blob.length()) : null);
	}

	/**
	 * Closes all the {@link ResultSet}, {@link Connection} and {@link Statement}.
	 */
	private void close ()
	{
		DatabaseConnection.closeAll(resultSet, statement, preparedStatement);
	}
}
