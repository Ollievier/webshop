package nl.olivier.app;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import nl.olivier.rest.*;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Rest application for a Webshop.
 */
@ApplicationPath("/")
public class RestApplication extends Application
{
	/**
	 * Constructor for the {@link Application} class.
	 * <p>
	 * Initializes the Swagger configuration.
	 * </p>
	 */
	public RestApplication ()
	{
		initializeSwaggerConfiguration();
	}

	/**
	 * Swagger configuration.
	 */
	private static void initializeSwaggerConfiguration ()
	{
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setSchemes(new String[]{"https"});
		beanConfig.setBasePath("rest/webshop/v1/");
		beanConfig.setScan(true);
		beanConfig.setPrettyPrint(true);
		beanConfig.setTitle("Webshop REST application");
	}

	/**
	 * Aggregates all resource files.
	 *
	 * @return Returns the resources for the REST-classes and Swagger configuration.
	 */
	@Override
	public Set<Class<?>> getClasses ()
	{
		Set<Class<?>> resources = new HashSet<Class<?>>();

		/* Resources containing Swagger */
		resources.add(SwaggerSerializers.class);

		/* Resources containing the endpoints */
		resources.add(ProductResource.class);
		resources.add(CategorieResource.class);
		resources.add(KlantResource.class);
		resources.add(AccountResource.class);
		resources.add(BestellingResource.class);

		return resources;
	}
}
