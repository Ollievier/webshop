package nl.olivier.services;

import nl.olivier.daos.BestellingDao;
import nl.olivier.models.Bestelling;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Service to manipulate {@link Bestelling}en.
 */
@Stateless
public class BestellingService
{
	@EJB
	private BestellingDao bestellingDao;

	/**
	 * Fetch all {@link Bestelling}en.
	 *
	 * @return A {@link List}<{@link Bestelling}>.
	 */
	public List<Bestelling> fetchBestellingen ()
	{
		return bestellingDao.fetchBestellingen();
	}


	/**
	 * @param bestellingId The {@link Bestelling} Id.
	 * @return A {@link Bestelling}.
	 */
	public Bestelling fetchBestelling (int bestellingId)
	{
		return bestellingDao.fetchBestelling(bestellingId);
	}

	/**
	 * Create a new {@link Bestelling}.
	 *
	 * @param bestelling The {@link Bestelling}.
	 * @return The resulting {@link Bestelling}.
	 */
	public Bestelling createBestelling (Bestelling bestelling)
	{
		return bestellingDao.createBestelling(bestelling);
	}

	/**
	 * Update a {@link Bestelling}.
	 *
	 * @param bestelling The {@link Bestelling}.
	 * @return The updated {@link Bestelling}.
	 */
	public Bestelling updateBestelling (Bestelling bestelling)
	{
		return bestellingDao.updateBestelling(bestelling);
	}

	/**
	 * Delete a {@link Bestelling}.
	 *
	 * @param bestellingId The {@link Bestelling} Id.
	 * @return Confirmation of deletion.
	 */
	public boolean deleteBestelling (int bestellingId)
	{
		return bestellingDao.deleteBestelling(bestellingId);
	}

	/**
	 * Checks if the {@link Bestelling} is updated with the new info.
	 */
	public boolean isEqual (Bestelling firstBestelling, Bestelling secondBestelling)
	{
		return firstBestelling.getId() == secondBestelling.getId()
				&& firstBestelling.getAccountId() == secondBestelling.getAccountId()
				&& firstBestelling.getAfleverAdres() == secondBestelling.getAfleverAdres()
				&& firstBestelling.getBestelDatum() == secondBestelling.getBestelDatum()
				&& firstBestelling.getTotaalPrijs() == secondBestelling.getTotaalPrijs();
	}
}
