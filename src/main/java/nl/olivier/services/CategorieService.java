package nl.olivier.services;

import nl.olivier.daos.CategorieDao;
import nl.olivier.models.Categorie;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Service to manipulate {@link Categorie}s.
 */
@Stateless
public class CategorieService
{
	@EJB
	private CategorieDao categorieDao;

	/**
	 * Fetch all {@link Categorie}s.
	 *
	 * @return A {@link List}<{@link Categorie}>.
	 */
	public List<Categorie> fetchCategories ()
	{
		return categorieDao.fetchCategories();
	}


	/**
	 * @param categorieId The {@link Categorie} Id.
	 * @return A {@link Categorie}.
	 */
	public Categorie fetchCategorie (int categorieId)
	{
		return categorieDao.fetchCategorie(categorieId);
	}

	/**
	 * Create a new {@link Categorie}.
	 *
	 * @param categorie The {@link Categorie}.
	 * @return The resulting {@link Categorie}.
	 */
	public Categorie createCategorie (Categorie categorie)
	{
		return categorieDao.createCategorie(categorie);
	}

	/**
	 * Update a {@link Categorie}.
	 *
	 * @param categorie The {@link Categorie}.
	 * @return The updated {@link Categorie}.
	 */
	public Categorie updateCategorie (Categorie categorie)
	{
		return categorieDao.updateCategorie(categorie);
	}

	/**
	 * Delete a {@link Categorie}.
	 *
	 * @param categorieId The {@link Categorie} Id.
	 * @return Confirmation of deletion.
	 */
	public boolean deleteCategorie (int categorieId)
	{
		return categorieDao.deleteCategorie(categorieId);
	}

	/**
	 * Checks if the {@link Categorie} is updated with the new info.
	 */
	public boolean isEqual (Categorie firstCategorie, Categorie secondCategorie)
	{
		return firstCategorie.getId() == secondCategorie.getId()
				&& firstCategorie.getNaam().equals(secondCategorie.getNaam())
				&& firstCategorie.getOmschrijving().equals(secondCategorie.getOmschrijving());
	}
}
