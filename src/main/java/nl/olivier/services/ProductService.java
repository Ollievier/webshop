package nl.olivier.services;

import nl.olivier.daos.ProductDao;
import nl.olivier.models.Categorie;
import nl.olivier.models.Product;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Service to manipulate {@link nl.olivier.models.Product}s.
 */
@Stateless
public class ProductService
{
	@EJB
	private ProductDao productDao;

	/**
	 * Fetch all {@link Product}s.
	 *
	 * @return A {@link java.util.List}<{@link Product}>.
	 */
	public List<Product> fetchProducts ()
	{
		return productDao.fetchProducts();
	}

	/**
	 * @param productId The {@link Product} Id.
	 * @return A {@link Product}.
	 */
	public Product fetchProduct (int productId)
	{
		return productDao.fetchProduct(productId);
	}

	/**
	 * Create a new {@link Product}.
	 *
	 * @param product The {@link Product}.
	 * @return The resulting {@link Product}.
	 */
	public Product createProduct (Product product)
	{
		return productDao.createProduct(product);
	}

	/**
	 * Update an existing {@link Product}.
	 *
	 * @param product The {@link Product}.
	 * @return The updated {@link Product}.
	 */
	public Product updateProduct (Product product)
	{
		return productDao.updateProduct(product);
	}

	/**
	 * Delete a {@link Product}.
	 *
	 * @param productId The {@link Product} Id.
	 * @return Confirmation of deletion.
	 */
	public boolean deleteProduct (int productId)
	{
		return productDao.deleteProduct(productId);
	}

	/**
	 * Checks if the {@link Product} is updated with the new info.
	 */
	public boolean isEqual (Product firstProduct, Product secondProduct)
	{
		return firstProduct.getId() == secondProduct.getId()
				&& firstProduct.getNaam().equals(secondProduct.getNaam())
				&& firstProduct.getPrijs() == secondProduct.getPrijs()
				&& firstProduct.getOmschrijving().equals(secondProduct.getOmschrijving());
	}

	/**
	 * Adds a {@link Categorie} to a {@link Product}.
	 */
	public Product addCategorieToProduct (Categorie categorie, int productId)
	{
		return productDao.addCategorieToProduct(categorie.getId(), productId);
	}

	/**
	 * Deletes a {@link Categorie} from a {@link Product}.
	 */
	public Product deleteCategorieFromProduct (Categorie categorie, int productId)
	{
		return productDao.deleteCategorieFromProduct(categorie.getId(), productId);
	}
}
