package nl.olivier.services;

import nl.olivier.daos.AccountDao;
import nl.olivier.models.Account;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Service to manipulate {@link Account}s.
 */
@Stateless
public class AccountService
{
	@EJB
	private AccountDao accountDao;

	/**
	 * Fetch all {@link Account}s.
	 *
	 * @return A {@link List}<{@link Account}>.
	 */
	public List<Account> fetchAccounts ()
	{
		return accountDao.fetchAccounts();
	}


	/**
	 * @param accountId The {@link Account} Id.
	 * @return A {@link Account}.
	 */
	public Account fetchAccount (int accountId)
	{
		return accountDao.fetchAccount(accountId);
	}

	/**
	 * Create a new {@link Account}.
	 *
	 * @param account The {@link Account}.
	 * @return The resulting {@link Account}.
	 */
	public Account createAccount (Account account)
	{
		return accountDao.createAccount(account);
	}

	/**
	 * Update a {@link Account}.
	 *
	 * @param account The {@link Account}.
	 * @return The updated {@link Account}.
	 */
	public Account updateAccount (Account account)
	{
		return accountDao.updateAccount(account);
	}

	/**
	 * Delete a {@link Account}.
	 *
	 * @param accountId The {@link Account} Id.
	 * @return Confirmation of deletion.
	 */
	public boolean deleteAccount (int accountId)
	{
		return accountDao.deleteAccount(accountId);
	}

	/**
	 * Checks if the {@link Account} is updated with the new info.
	 */
	public boolean isEqual (Account firstAccount, Account secondAccount)
	{
		return firstAccount.getId() == secondAccount.getId()
				&& firstAccount.isActief() == secondAccount.isActief()
				&& firstAccount.getOpenDatum() == secondAccount.getOpenDatum()
				&& firstAccount.getKlantId() == secondAccount.getKlantId()
				&& firstAccount.getFactuurAdresId() == secondAccount.getFactuurAdresId();
	}
}
