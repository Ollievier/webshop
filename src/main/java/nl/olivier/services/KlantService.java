package nl.olivier.services;

import nl.olivier.daos.KlantDao;
import nl.olivier.models.Klant;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Service to manipulate {@link Klant}en.
 */
@Stateless
public class KlantService
{
	@EJB
	private KlantDao klantDao;

	/**
	 * Fetch all {@link Klant}en.
	 *
	 * @return A {@link List}<{@link Klant}>.
	 */
	public List<Klant> fetchKlanten ()
	{
		return klantDao.fetchKlanten();
	}


	/**
	 * @param klantId The {@link Klant} Id.
	 * @return A {@link Klant}.
	 */
	public Klant fetchKlant (int klantId)
	{
		return klantDao.fetchKlant(klantId);
	}

	/**
	 * Create a new {@link Klant}.
	 *
	 * @param klant The {@link Klant}.
	 * @return The resulting {@link Klant}.
	 */
	public Klant createKlant (Klant klant)
	{
		return klantDao.createKlant(klant);
	}

	/**
	 * Update a {@link Klant}.
	 *
	 * @param klant The {@link Klant}.
	 * @return The updated {@link Klant}.
	 */
	public Klant updateKlant (Klant klant)
	{
		return klantDao.updateKlant(klant);
	}

	/**
	 * Delete a {@link Klant}.
	 *
	 * @param klantId The {@link Klant} Id.
	 * @return Confirmation of deletion.
	 */
	public boolean deleteKlant (int klantId)
	{
		return klantDao.deleteKlant(klantId);
	}

	/**
	 * Checks if the {@link Klant} is updated with the new info.
	 */
	public boolean isEqual (Klant firstKlant, Klant secondKlant)
	{
		return firstKlant.getId() == secondKlant.getId()
				&& firstKlant.getNaam().equals(secondKlant.getNaam())
				&& firstKlant.getWoonAdresId() == secondKlant.getWoonAdresId();
	}
}
